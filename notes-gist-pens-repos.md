# Notes - Gist - Pens - Repos

### My website : 
- [link](https://haikel-fazzani.netlify.com)

### algorithms practice
- [link](https://gitlab.com/haikelfazzani/code-challenges)

### techniques && approaches
- [Fuzzy match](https://github.com/haikelfazzani/notes-gist-pens-repos/tree/master/fuzzy-match)
- [Js memoization](https://codepen.io/haikelfazzani-the-bold/pen/GRgERyx)

### fontend
- [MutationObserver](https://codepen.io/haikelfazzani-the-bold/pen/LYEJyqw)
- [Nice switch](https://codepen.io/haikelfazzani-the-bold/pen/LYEOKmK)
- [Dynamic tabs](https://codepen.io/haikelfazzani-the-bold/pen/MWYoxVM)
- [Input autocomplete](https://codepen.io/haikelfazzani-the-bold/pen/yLybKmO)
- [Nice weather widget](https://codepen.io/haikelfazzani-the-bold/pen/XWJzqWw)
- [Sortable List using JavaScript Drag and Drop API](https://codepen.io/haikelfazzani-the-bold/pen/zYxwzRK)
- [Js highlights](https://codepen.io/haikelfazzani-the-bold/pen/XWrKzdw)

### Learn more
- Learn more about gRPC
- Learn more about PWA

### Packages
- [Link](https://github.com/wutility)

### Browser extensions
- [Link](https://github.com/haikelfazzani/chrome-extensions)
- [Link](https://github.com/Chromo-lib)